﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerController : NetworkBehaviour {

    public float walkingSpeed = 100f;
    public float rotateSpeed = 8f;
    public GameObject playerIndicator;
    public Vector3 indicatorOffset = new Vector3(0, 0.5f, 0);
    public Health healthManager;
    public GameObject simpleRange;
    public bool canAttack = true;

    public void Start()
    {
        healthManager = GetComponent<Health>();
        healthManager.localStart = transform;
        
        simpleRange = transform.Find("attackRange").gameObject;
    }

    public override void OnStartLocalPlayer()
    {
        if (playerIndicator == null)
        {
            Debug.Log("Please set a player indicator");
            var indicSurrogate = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            indicSurrogate.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
            indicSurrogate.transform.position = Vector3.zero;
            playerIndicator = indicSurrogate;


            indicSurrogate.SetActive(false);           
        }

        var indicator = (GameObject)Instantiate(playerIndicator, transform.position + indicatorOffset, transform.rotation, this.transform);
        indicator.SetActive(true);
        
        indicator.GetComponent<MeshRenderer>().material.color = Color.green;
    }

    void Update()
    {
        if (!isLocalPlayer)
        {
            return;
        }

        var x = Input.GetAxis("Horizontal") * Time.deltaTime * rotateSpeed * rotateSpeed;
        var z = Input.GetAxis("Vertical") * Time.deltaTime * walkingSpeed;

        transform.Rotate(0, x, 0);
        transform.Translate(0, 0, z);

        if (Input.GetKeyDown(KeyCode.Space) && canAttack)
        {
            Debug.Log("Space input OK, start command.");
            CmdSimpleAttack();
        }
    }

    [Command]
    void CmdSimpleAttack()
    {
        Debug.Log("Command launched, starting coroutine on clients...");
        StartCoroutine(SimpleAttack());
    }

    IEnumerator SimpleAttack()
    {
        Debug.Log("Coroutine succeeded, enabling attack range...");
        canAttack = false;
        simpleRange.SetActive(true);
        yield return new WaitForSeconds(1);
        canAttack = true;
        simpleRange.SetActive(false);

    }
}
