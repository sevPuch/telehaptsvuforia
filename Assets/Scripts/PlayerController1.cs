﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerController1 : NetworkBehaviour
{

    public float walkingSpeed = 100f;
    public float rotateSpeed = 8f;
    public GameObject playerIndicator;
    public Vector3 indicatorOffset = new Vector3(0, 0.5f, 0);
    public Health1 healthManager;
    public EnemySpawner enemySpawner;
    public GameObject simpleRange;
    public bool canAttack = true;

    public GameObject bulletPrefab;
    public Transform bulletSpawn;

    public GameManager gameManager;
    public Button readyButton;
    int startCountdown = 1;
    public GameObject canvas;

    public void Start()
    {
        healthManager = GetComponent<Health1>();
        enemySpawner = FindObjectOfType<EnemySpawner>();
 
        simpleRange = transform.Find("attackRange").gameObject;
        //add this player to the list of players
        gameManager = FindObjectOfType<GameManager>();
        gameManager.players.Add(gameObject);
        gameManager.networkIsReady = true;

        Debug.Log("At least one player found - network is ready !");

    }

    public override void OnStartLocalPlayer()
    {
        readyButton = transform.Find("ScreenCanvas/Container/readyButton").GetComponent<Button>();
        readyButton.enabled = true;
        readyButton.onClick.AddListener(delegate { ButtonListener(); });
     
        if (playerIndicator == null)
        {
            Debug.Log("Please set a player indicator");
            var indicSurrogate = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            indicSurrogate.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
            indicSurrogate.transform.position = Vector3.zero;
            playerIndicator = indicSurrogate;

            indicSurrogate.SetActive(false);           
        }

        var indicator = (GameObject)Instantiate(playerIndicator, transform.position + indicatorOffset, transform.rotation, this.transform);
        indicator.SetActive(true);
        
        indicator.GetComponent<MeshRenderer>().material.color = Color.green;
    }

    public void ButtonListener()
    {
        DisableReadyButton(readyButton);
        CmdAddPlayer();
    }

    [Command]
    public void CmdAddPlayer()
    {
        Debug.Log("Command launched");
        gameManager.readyCount += 1;
        
    }

    public void DisableReadyButton(Button _readyButton)
    {
        if (!isLocalPlayer) return;
        
        _readyButton.interactable = false;
        _readyButton.GetComponentInChildren<Text>().text = "皆の準備を待っています...";
        _readyButton.GetComponentInChildren<Text>().color = Color.white;
    }

    [ClientRpc]
    public void RpcCountdownButton()
    {
        if (!isLocalPlayer) return;
        StartCoroutine(CountdownCoroutine());
    }

    IEnumerator CountdownCoroutine()
    {
        readyButton.GetComponentInChildren<Text>().text = "Start in " + startCountdown + " seconds !";
        yield return new WaitForSeconds(1);

        for (int i = startCountdown; i > 0; i--)
        {
            readyButton.GetComponentInChildren<Text>().text = "Start in " + (i - 1) + " seconds !";
            yield return new WaitForSeconds(1);
        }
        readyButton.gameObject.SetActive(false);

        if (isServer)
        {
            gameManager.canSpawn = true;
            gameManager.IncrementWave();
        }
    }

    void Update()
    {
        if (!isLocalPlayer)
        {
            Debug.Log("This isnt local player, destroy start button " + canvas);
            Destroy(canvas);
            return;
        }

        var x = Input.GetAxis("Horizontal") * Time.deltaTime * rotateSpeed * rotateSpeed;
        var z = Input.GetAxis("Vertical") * Time.deltaTime * walkingSpeed;

        transform.Rotate(0, x, 0);
        transform.Translate(0, 0, z);

        if (Input.GetKeyDown(KeyCode.Space) && canAttack)
        {
            Debug.Log("Space input OK, start command.");
            CmdSimpleAttack();
        }

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            CmdFire();
        }
    }

    [Command]
    void CmdFire()
    {
        // Create the Bullet from the Bullet Prefab
        var bullet = (GameObject)Instantiate(
            bulletPrefab,
            bulletSpawn.position,
            bulletSpawn.rotation);

        // Add velocity to the bullet
        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 6;

        NetworkServer.Spawn(bullet);
        
        // Destroy the bullet after 2 seconds
        Destroy(bullet, 2.0f);
    }

    [Command]
    void CmdSimpleAttack()
    {
        Debug.Log("Command launched, starting coroutine on clients...");
        StartCoroutine(SimpleAttack());
    }

    IEnumerator SimpleAttack()
    {
        Debug.Log("Coroutine succeeded, enabling attack range...");
        canAttack = false;
        simpleRange.SetActive(true);
        yield return new WaitForSeconds(1);
        canAttack = true;
        simpleRange.SetActive(false);
    }
}
