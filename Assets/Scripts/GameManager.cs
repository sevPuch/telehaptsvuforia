﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class GameManager : NetworkBehaviour {

    public List<GameObject> players;
    public EnemySpawner enemySpawner;
    [SyncVar]
    public int readyCount = 0;
    [SyncVar]
    public bool ready = false;
    [SyncVar]
    public int wave = 0;


    public bool networkIsReady = false;
    public bool canSpawn = false;


    void Start () {
        players = new List<GameObject>();
        enemySpawner = FindObjectOfType<EnemySpawner>();
    }

    [Server]
    void Update()
    {
        if (networkIsReady == false) return;
        if (!ready)
        {
            Debug.Log("Players count is : " + players.Count);
            if (readyCount >= players.Count)
            {
                Debug.Log("All players are ready, start the game. Laumch RPC.");
                ready = true;
                LaunchGame();
            }
            Debug.Log("Players not ready, return Update");
            return;
        }
        if (!canSpawn) return;        

        switch (wave)
        {
            case 1:
                enemySpawner.SpawnWave(25);                
                break;

            case 2:
                enemySpawner.SpawnWave(8);
                break;
        }
    }
    
    public void LaunchGame()
    {
        Debug.Log("Game launched on the server, call local countdowns...");
        foreach(GameObject _player in players)
        {
            Debug.Log(_player.name);
            _player.GetComponent<PlayerController1>().RpcCountdownButton();
        }
    }

    public void IncrementWave()
    {
        ++wave;
    }
    
}
