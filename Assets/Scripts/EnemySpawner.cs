﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class EnemySpawner : NetworkBehaviour
{
    public GameObject enemyPrefab;
    public GameManager manager;

    public List<GameObject> enemies;

    public void Start()
    {
        manager = FindObjectOfType<GameManager>();
    }

    public void SpawnWave(int _number)
    {
        manager.canSpawn = false;
        enemies = new List<GameObject>();

        int rowCount = 0;
        int maxRowPlusOne = 4;
        float _xOffset = 0f;
        float _zOffset = 0f;
        float _width = 0.6f;

        for (int i = 0; i < _number; i++)
        {
            ++rowCount;
            Vector3 spawnPosition;
            Quaternion spawnRotation;

            if (rowCount < maxRowPlusOne)
            {
                spawnPosition = new Vector3(_xOffset, 0, _zOffset);
                spawnRotation = Quaternion.Euler(0.0f, Random.Range(0, 180), 0.0f);

                var enemy = (GameObject)Instantiate(enemyPrefab, spawnPosition, spawnRotation);
                NetworkServer.Spawn(enemy);
                enemies.Add(enemy);
                enemy.SetActive(true);
                _xOffset += _width;
                //_xOffset = MeasureBounds(enemyPrefab, _xOffset);

            }
            else
            {
                rowCount = 1;
                _xOffset = 0f;
                //_zOffset = MeasureBounds(enemyPrefab, _zOffset);
                _zOffset += _width;

                spawnPosition = new Vector3(_xOffset, 0, _zOffset);
                spawnRotation = Quaternion.Euler(0.0f, Random.Range(0, 180), 0.0f);
                var enemy = (GameObject)Instantiate(enemyPrefab, spawnPosition, spawnRotation);
                NetworkServer.Spawn(enemy);
                enemies.Add(enemy);

                _xOffset += _width;
            }
        }

        _xOffset = 0f;
        _zOffset = 0f;
        rowCount = 0;
    }

    /*
    public float MeasureBounds(GameObject _enemy, float offset)
    {
        Collider _col = _enemy.GetComponentInChildren<Collider>();
        Debug.Log(_col);
        if (_col.bounds.size.x > _col.bounds.size.z)
        {
            Debug.Log(_col.bounds.size);
            offset += _col.bounds.size.x;
            return offset;
        }
        else
        {
            Debug.Log(_col.bounds.size);
            offset += _col.bounds.size.z;
            return offset;
        }
    }
    */
}
