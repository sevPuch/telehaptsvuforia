﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class UIManager : NetworkBehaviour
{
    public GameObject canvas;

    public void Awake()
    {
        Debug.Log("Started on client, checking UI");
        if (isLocalPlayer)
        {
            Debug.Log("This isnt local player, destroy start button " + canvas);
            Destroy(canvas);
            Debug.Log(canvas);
        }
    }
}
