﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;

public class Health : NetworkBehaviour
{

    public const int maxHealth = 100;
    public bool destroyOnDeath;
    public Transform localStart;

    [SyncVar(hook = "OnChangeHealth")]
    public int currentHealth = maxHealth;

    public RectTransform healthBar;

    private NetworkStartPosition[] spawnPoints;

    void Start()
    {
        if (isLocalPlayer)
        {
            spawnPoints = FindObjectsOfType<NetworkStartPosition>();
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        Debug.Log("Collision detected, testing other collider...");
        if (other.tag == "attack" && other.gameObject != transform.Find("attackRange").gameObject)
        {
            Debug.Log("Take that m*therf*cker!");
            TakeDamage(30);
        }
    }

    public void TakeDamage(int amount)
    {
        if (!isServer) {
            Debug.Log("!isServer, return");
            return;
        }

        currentHealth -= amount;
        if (currentHealth <= 0)
        {
            if (destroyOnDeath)
            {
                Destroy(gameObject);
            }
            else
            {
                currentHealth = maxHealth;

                // called on the Server, invoked on the Clients
                RpcRespawn();
            }
        }
    }

    void OnChangeHealth(int currentHealth)
    {
        //healthBar.sizeDelta = new Vector2(currentHealth, healthBar.sizeDelta.y);
    }

    [ClientRpc]
    void RpcRespawn()
    {
        if (isLocalPlayer)
        {
            if (spawnPoints == null || spawnPoints.Length == 0)
            {
                transform.position = Vector3.zero;
            }

            // Set the player’s position to the chosen spawn point
            transform.position = localStart.position;
            transform.rotation = localStart.rotation;
        }
    }
}