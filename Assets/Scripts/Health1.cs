﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;

public class Health1 : NetworkBehaviour
{
    public const int maxHealth = 100;
    public Vector3 spawnPoint;
    public Quaternion spawnRotation;
    public bool destroyOnDeath;

    [SyncVar(hook = "OnChangeHealth")]
    public int currentHealth = maxHealth;

    public RectTransform healthBar;
 
    public override void OnStartLocalPlayer()
    {
        spawnPoint = transform.position;
        spawnRotation = transform.rotation;
        GameObject respawnToken = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        respawnToken.transform.position = spawnPoint;
        respawnToken.GetComponent<MeshRenderer>().material.color = Color.blue;
        respawnToken.GetComponent<SphereCollider>().enabled = false;
    }

    public void TakeDamage(int amount)
    {
        if (!isServer)
            return;

        currentHealth -= amount;
        if (currentHealth <= 0)
        {
            if (destroyOnDeath)
            {
                Destroy(gameObject);
            }
            else
            {
                currentHealth = maxHealth;
                RpcRespawn();
            }
        }
    }

    [ClientRpc]
    void RpcRespawn()
    {
        Debug.Log("Launched RPC");
        if (isLocalPlayer)
        {
            Debug.Log("RPC is local player");
            Debug.Log(spawnPoint);


            transform.position = spawnPoint;
            transform.rotation = spawnRotation;
        }
    }

    void OnChangeHealth(int health)
    {
        healthBar.sizeDelta = new Vector2(health, healthBar.sizeDelta.y);
    }


    public void OnTriggerEnter(Collider other)
    {
        Debug.Log("Collision detected, testing other collider...");
        if (other.tag == "attack" && other.gameObject != transform.Find("attackRange").gameObject)
        {
            Debug.Log("Take that m*therf*cker!");
            TakeDamage(30);
        }
    }
}